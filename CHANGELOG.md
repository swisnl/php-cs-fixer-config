# Changelog

All notable changes to `swisnl/php-cs-fixer-config` will be documented in this file.

Updates should follow the [Keep a CHANGELOG](http://keepachangelog.com/) principles.


## 1.38.0 - 2025-07-31

### Changed
- Require PHP-CS-Fixer 3.70.*


## 1.37.0 - 2025-01-31

### Changed
- Require PHP-CS-Fixer 3.68.*


## 1.36.0 - 2024-09-16

### Changed
- Require PHP-CS-Fixer 3.64.*
- Enable parallel runners


## 1.35.0 - 2024-06-26

### Changed
- Require PHP-CS-Fixer 3.59.*


## 1.34.0 - 2024-04-19

### Changed
- Require PHP-CS-Fixer 3.54.*


## 1.33.0 - 2024-03-20

### Changed
- Require PHP-CS-Fixer 3.52.*


## 1.32.0 - 2024-02-23

### Changed
- Require PHP-CS-Fixer 3.49.*


## 1.31.0 - 2024-01-16

### Changed
- Require PHP-CS-Fixer 3.47.*


## 1.30.0 - 2023-12-13

### Changed
- Require PHP-CS-Fixer 3.41.*


## 1.29.0 - 2023-11-27

### Changed
- Require PHP-CS-Fixer 3.40.*


## 1.28.0 - 2023-11-24

### Changed
- Require PHP-CS-Fixer 3.39.*


## 1.27.0 - 2023-11-14

### Changed
- Require PHP-CS-Fixer 3.38.*


## 1.26.0 - 2023-11-01

### Changed
- Require PHP-CS-Fixer 3.37.*


## 1.25.0 - 2023-10-13

### Changed
- Require PHP-CS-Fixer 3.35.*


## 1.24.0 - 2023-10-11

### Changed
- Require PHP-CS-Fixer 3.34.*


## 1.23.0 - 2023-09-21

### Changed
- Require PHP-CS-Fixer 3.27.*


## 1.22.0 - 2023-09-04

### Changed
- Require PHP-CS-Fixer 3.25.*


## 1.21.0 - 2023-07-24

### Changed
- Require PHP-CS-Fixer 3.22.*


## 1.20.0 - 2023-07-14

### Changed
- Require PHP-CS-Fixer 3.21.*
- Bump PHP requirement to 7.4


## 1.19.0 - 2023-06-30

### Changed
- Require PHP-CS-Fixer 3.20.*


## 1.18.0 - 2023-04-07

### Changed
- Require PHP-CS-Fixer 3.16.*


## 1.17.0 - 2023-03-27

### Changed
- Require PHP-CS-Fixer 3.15.*


## 1.16.0 - 2023-01-31

### Changed
- Require PHP-CS-Fixer 3.14.*


## 1.15.0 - 2022-11-02

### Changed
- Require PHP-CS-Fixer 3.13.*
- Set config value for `global_namespace_import`


## 1.14.0 - 2022-10-18

### Changed
- Require PHP-CS-Fixer 3.12.*


## 1.13.0 - 2022-09-13

### Changed
- Require PHP-CS-Fixer 3.11.*


## 1.12.0 - 2022-08-19

### Changed
- Require PHP-CS-Fixer 3.10.*


## 1.11.0 - 2022-03-22

### Added
- Add rule to force using FQCN in DocBlocks


## 1.10.0 - 2022-03-21

### Changed
- Require PHP-CS-Fixer 3.8.*


## 1.9.0 - 2022-03-09

### Changed
- Require PHP-CS-Fixer 3.7.*


## 1.8.0 - 2022-02-08

### Changed
- Require PHP-CS-Fixer 3.6.*


## 1.7.0 - 2021-12-14

### Changed
- Require PHP-CS-Fixer 3.4.*


## 1.6.0 - 2021-11-18

### Changed
- Require PHP-CS-Fixer 3.3.*


## 1.5.0 - 2021-10-13

### Changed
- Require PHP-CS-Fixer 3.2.*


## 1.4.0 - 2021-09-01

### Changed
- Require PHP-CS-Fixer 3.1.*


## 1.3.0 - 2021-05-10

### Changed
- Require PHP-CS-Fixer 3.0.*


## 1.2.2 - 2021-03-22

### Fixed
- Do not use deprecated static create method of config


## 1.2.1 - 2021-03-15

### Fixed
- Fixed the config for `yoda_style` rule


## 1.2.0 - 2021-01-05

### Changed
- Disabled some rules introduced in PHP-CS-Fixer [2.17](https://github.com/FriendsOfPHP/PHP-CS-Fixer/releases/tag/v2.17.0) (Desert Beast)


## 1.1.0 - 2019-11-06

### Changed
- Disabled some rules introduced in PHP-CS-Fixer [2.16](https://github.com/FriendsOfPHP/PHP-CS-Fixer/releases/tag/v2.16.0) (Yellow Bird)
