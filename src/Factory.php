<?php

namespace Swis\PhpCsFixer\Config;

use AdamWojs\PhpCsFixerPhpdocForceFQCN\Fixer\Phpdoc\ForceFQCNFixer;
use PhpCsFixer\Config;
use PhpCsFixer\ConfigInterface;
use PhpCsFixer\Runner\Parallel\ParallelConfigFactory;

class Factory
{
    /**
     * @return \PhpCsFixer\ConfigInterface
     */
    public static function create(): ConfigInterface
    {
        return (new Config())
            ->setParallelConfig(ParallelConfigFactory::detect())
            ->registerCustomFixers([
                new ForceFQCNFixer(),
            ])
            ->setRules(
                [
                    '@Symfony' => true,
                    'array_syntax' => ['syntax' => 'short'],
                    'linebreak_after_opening_tag' => true,
                    'ordered_imports' => true,
                    'phpdoc_add_missing_param_annotation' => true,
                    'phpdoc_order' => true,
                    'yoda_style' => false,
                    'no_superfluous_phpdoc_tags' => false,
                    'single_line_throw' => false,
                    'php_unit_method_casing' => false,
                    'global_namespace_import' => [
                        'import_classes' => true,
                        'import_constants' => false,
                        'import_functions' => false,
                    ],
                    'AdamWojs/phpdoc_force_fqcn_fixer' => true,
                    'nullable_type_declaration_for_default_null_value' => true,
                    'fully_qualified_strict_types' => false,
                ]
            );
    }
}
