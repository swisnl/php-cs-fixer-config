# swisnl/php-cs-fixer-config

Default PHP-CS-Fixer config factory used at SWIS


## Install

Via Composer

``` bash
composer require --dev swisnl/php-cs-fixer-config
```

## Usage

Just use the config factory in your PHP-CS-Fixer config, and you're good to go!

``` php
<?php

$finder = PhpCsFixer\Finder::create()
    ->in([
        __DIR__.'/app',
        __DIR__.'/config',
        __DIR__.'/database',
        __DIR__.'/lang',
        __DIR__.'/routes',
        __DIR__.'/tests',
    ])
;

return Swis\PhpCsFixer\Config\Factory::create()
    ->setFinder($finder);
```

## Change log

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

### How to release a new version

1. Read through the [release notes of PHP-CS-Fixer](https://github.com/PHP-CS-Fixer/PHP-CS-Fixer/releases) for all versions between the current version and the latest version to see if something noticeable has changed.
2. Install the latest version of PHP-CS-Fixer in an existing (big) project by using an alias. The first version is the one you want to test and the second version is the currently installed one.
``` json
"require-dev": {
    "friendsofphp/php-cs-fixer": "3.34.1 as 3.27.0"
}
```
3. Run `composer update friendsofphp/php-cs-fixer`.
4. Run PHP-CS-Fixer.
5. Check to see if no unexpected or undesired changes have been made. If so, they can be either caused by a new feature we don't like or a bug in PHP-CS-Fixer. If it's a new feature we want to disable, update the config in `src/Factory.php`. If it's a bug, please report it to the PHP-CS-Fixer repository and wait for it to be fixed.
6. Revert the changes made to the project and close it.
7. Bump the PHP-CS-Fixer version in `composer.json` and commit.
8. Update the release notes in `CHANGELOG.md` and commit.
9. Tag the latest commit and push the changes. Don't forget to push the tags!

## Security

If you discover any security related issues, please email [security@swis.nl](mailto:security@swis.nl) instead of using the issue tracker.

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
