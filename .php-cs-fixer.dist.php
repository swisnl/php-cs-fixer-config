<?php

$finder = PhpCsFixer\Finder::create()
    ->in([
        __DIR__.'/src'
    ])
;

return Swis\PhpCsFixer\Config\Factory::create()
    ->setFinder($finder);
